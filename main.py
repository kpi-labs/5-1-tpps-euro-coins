from euroCoins.CountriesMap import CountriesMap
from euroCoins.Simulation import Simulation
from euroCoins.constants import CASES_FILENAME
from euroCoins.fs import read_test_cases


def main():
    i = 0
    for data in read_test_cases(CASES_FILENAME):
        i += 1
        print(f'Case Number {i}')
        try:
            simulation = Simulation(CountriesMap.deserialize(data))
        except ValueError as error:
            print(f'Error!: {error.args[0]}')
            continue
        print(simulation.countries_map)
        result = sorted(
            simulation.simulate().items(),
            key=lambda d: (d[0].name, d[1])
        )

        for country, days in result:
            print(f'{country} {days}')


if __name__ == '__main__':
    main()
