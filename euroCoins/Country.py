from euroCoins.Point import Point
from euroCoins.Rectangle import Rectangle
from euroCoins.City import City
import re


class Country:
    def __init__(self, name, area):
        self.bounds = area
        self.name = name
        self.cities = []
        self._fill_cities()

    def _fill_cities(self):
        for point in self.bounds.inner_points():
            self.cities.append(City(point, self))

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        return self.name == other.name

    def is_completed(self, other_countries_names):
        return all(
            [city.is_completed(other_countries_names)
             for city in self.cities]
        )

    def __repr__(self):
        return self.name

    @staticmethod
    def deserialize(data):
        if not re.match(r'([A-Z]+)(?:\s(\d)+){4}', data, re.IGNORECASE):
            raise ValueError(f'Cannot deserialize country from "{data}"')

        groups = data.split(' ')
        country_name = ' '.join(groups[0:-4])
        y1 = int(groups[-1])
        x1 = int(groups[-2])
        y0 = int(groups[-3])
        x0 = int(groups[-4])
        return Country(country_name, Rectangle(Point(x0, y0), Point(x1, y1)))
