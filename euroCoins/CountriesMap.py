from operator import attrgetter
from euroCoins.Country import Country
from euroCoins.Point import Point


class CountriesMap:
    def __init__(self, countries):
        if len(countries) != len(set(map(attrgetter('name'), countries))):
            raise ValueError('Map contains countries with the same names')

        self.countries = countries
        self.cities_grid = {}
        self._fill_cities_map()
        self._validate_countries_borders()

    def _fill_cities_map(self):
        for country in self.countries:
            for city in country.cities:
                if city.point in self:
                    raise ValueError(
                        'Countries are intercepting'
                        f'{self.cities_grid[city.point].country.name}'
                        f' and {city.country.name} at {city.point}'
                    )
                self.cities_grid[city.point] = city

    def _city_has_foreign_neighbors(self, city):
        return next(
            (
                True
                for neighbor in city.point.neighbors
                if (
                    neighbor in self
                    and self[neighbor].country.name != city.country.name
                )
            ),
            None) is not None

    def _validate_countries_borders(self):
        if len(self.countries) == 1:
            return

        for country in self.countries:
            top_left = country.bounds.top_left
            bottom_right = country.bounds.bottom_right
            for x in range(top_left.x, bottom_right.x+1):
                if (
                    self._city_has_foreign_neighbors(
                        self[Point(x, top_left.y)])
                    or
                    self._city_has_foreign_neighbors(
                        self[Point(x, bottom_right.y)]
                    )
                ):
                    break
            else:
                for y in range(top_left.y+1, bottom_right.y):
                    if (
                        self._city_has_foreign_neighbors(
                            self[Point(top_left.x, y)])
                        or
                        self._city_has_foreign_neighbors(
                            self[Point(bottom_right.x, y)]
                        )
                    ):
                        break
                else:
                    raise ValueError(
                        f'{country.name} has no neighbors on the map')

    def __str__(self):
        min_x = min(map(lambda c: c.bounds.top_left.x, self.countries))
        min_y = min(map(lambda c: c.bounds.top_left.y, self.countries))
        max_x = max(map(lambda c: c.bounds.bottom_right.x, self.countries))
        max_y = max(map(lambda c: c.bounds.bottom_right.y, self.countries))
        repr = f'({max_y},{min_y})\n'
        for y in range(min_y, max_y+1):
            for x in range(min_x, max_x+1):
                coords = Point(x, y)
                if coords in self.cities_grid:
                    repr += self.cities_grid[coords].country.name[0]
                else:
                    repr += '-'
                if x < max_x:
                    repr += ' '
            repr += '\n'
        right_bottom_coords = f'({max_x},{max_y})'
        repr += (' ')*max(len(max(repr.split('\n'))) -
                          len(right_bottom_coords), 0)
        repr += right_bottom_coords
        return repr

    def __contains__(self, point):
        return point in self.cities_grid

    def __getitem__(self, point):
        return self.cities_grid.get(point)

    @ staticmethod
    def deserialize(data):
        return CountriesMap(
            [Country.deserialize(country_data) for country_data in data]
        )
