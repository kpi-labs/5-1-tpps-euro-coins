from euroCoins.constants import MAX_X, MAX_Y, MIN_X, MIN_Y
from euroCoins.Point import Point


class Rectangle:
    def __init__(self, top_left, bottom_right):
        self._validate_args(top_left, bottom_right)
        self.top_left = top_left
        self.bottom_right = bottom_right

    def inner_points(self):
        for x in range(self.top_left.x, self.bottom_right.x+1):
            for y in range(self.top_left.y, self.bottom_right.y+1):
                yield Point(x, y)

    def _validate_args(self, top_left, bottom_right):
        for coords in [top_left, bottom_right]:
            if coords.x < MIN_X or coords.x > MAX_X:
                raise ValueError(f'X({coords.x}) is out of allowed bounds')
            if coords.y < MIN_Y or coords.y > MAX_Y:
                raise ValueError(f'Y({coords.y}) is out of allowed bounds')

        if top_left.x > bottom_right.x:
            raise ValueError(
                f'Top left X({top_left.x}) should be lower'
                f' than bottom right X{bottom_right.x}'
            )

        if top_left.y > bottom_right.y:
            raise ValueError(
                f'Top left Y({top_left.y}) should be lower'
                f' than bottom right Y({bottom_right.y})'
            )
