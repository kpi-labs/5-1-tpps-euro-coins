class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __hash__(self):
        return hash(str(self))

    def __repr__(self):
        return f'{self.x}-{self.y}'

    def __eq__(self, other):
        return other and self.x == other.x and self.y == other.y

    @property
    def neighbors(self):
        return [
            Point(self.x-1, self.y),
            Point(self.x, self.y-1),
            Point(self.x+1, self.y),
            Point(self.x, self.y+1),
        ]
