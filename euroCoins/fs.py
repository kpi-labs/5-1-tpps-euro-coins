def read_test_cases(filename):
    with open(filename, 'r') as file_content:
        while True:
            next_countries_count_line = file_content.readline()

            if len(next_countries_count_line) == 0:
                break
            try:
                next_countries_count = int(next_countries_count_line)
            except ValueError as error:
                raise ValueError(
                    f'Line with countries count '
                    f'contains non-integer value {error}'
                )

            if next_countries_count > 20:
                for _ in range(next_countries_count):
                    file_content.readline().rstrip()
                raise ValueError(
                    f'Too many countries ({next_countries_count}) in case')

            if next_countries_count == 0:
                break

            yield [
                file_content.readline().rstrip()
                for _ in range(next_countries_count)
            ]
