import os
import pathlib

curr_dir = pathlib.Path(__file__).parent.absolute()


INITIAL_COINS = 1000000
CASES_FILENAME = os.path.join(curr_dir, '../data/cases.txt')
MIN_X = 1
MAX_X = 10
MIN_Y = 1
MAX_Y = 10
CITY_PORTION_FACTOR = 1000
