class Simulation:
    def __init__(self, countries_map):
        self.countries_map = countries_map
        self.iterations = 0

    def make_iteration(self):
        changes = []
        for city in self.countries_map.cities_grid.values():
            portion = city.next_portion()
            changes.append((city, portion))

        for change in changes:
            city, portion = change
            for city_neighbors_coords in city.point.neighbors:
                if city_neighbors_coords in self.countries_map:
                    city.substract_portion(portion)
                    self.countries_map[city_neighbors_coords].add_portion(
                        portion)

    def get_completed_countries(self):
        completed_countries = {}
        all_country_names = list(
            map(lambda c: c.name, self.countries_map.countries))

        for country in self.countries_map.countries:
            if country.is_completed(all_country_names):
                completed_countries[country] = self.iterations

        return completed_countries

    def simulate(self):
        completed_countries = self.get_completed_countries()

        while True:
            self.iterations += 1
            self.make_iteration()
            completed_countries = \
                self.get_completed_countries() | completed_countries

            if len(completed_countries) == len(self.countries_map.countries):
                break

        return completed_countries
