from euroCoins.constants import CITY_PORTION_FACTOR, INITIAL_COINS


class City:
    def __init__(self, point, country):
        self.point = point
        self.country = country
        self.bank = {country.name: INITIAL_COINS}

    def next_portion(self):
        portion = {
            motif: int(balance/CITY_PORTION_FACTOR)
            for motif, balance in self.bank.items()
        }

        return portion

    def substract_portion(self, portion):
        for motif, balance in portion.items():
            self.bank[motif] -= balance

    def add_portion(self, portion):
        for motif, balance in portion.items():
            self.bank[motif] = self.bank.get(motif, 0) + balance

    def is_completed(self, countries_names):
        return all(
            map(
                lambda country_name: self.bank.get(country_name, 0) > 0,
                countries_names
            )
        )
