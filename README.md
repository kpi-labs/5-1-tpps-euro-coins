# Euro coins defusion simulation

Required Python >= 3.9

## How to run

```bash
python3 main.py
```

## Expected output

```
2
Belgium 3 3 3 3
Belgium 3 3 3 3
2
Belgium 0 3 3 3
Italia 3 3 3 3
1
Belgium 0 3 sd 3
3
France 1 4 4 6
Spain 3 1 6 3
Portugal 1 1 2 2
1
Luxembourg 1 1 1 1
2
Netherlands 1 3 2 4
Belgium 1 1 2 2
2
Netherlands 1 1 1 1
Belgium 2 1 2 1
2
Netherlands 1 1 1 1
Belgium 1 1 1 1
2
Netherlands 1 5 1 2
Belgium 3 3 3 3
2
Netherlands 1 1 1 1
Belgium 3 3 3 3
21
Belgium 3 3 3 3
Belgium 3 3 3 3
Belgium 3 3 3 3
Belgium 3 3 3 3
Belgium 3 3 3 3
Belgium 3 3 3 3
Belgium 3 3 3 3
Belgium 3 3 3 3
Belgium 3 3 3 3
Belgium 3 3 3 3
Belgium 3 3 3 3
Belgium 3 3 3 3
Belgium 3 3 3 3
Belgium 3 3 3 3
Belgium 3 3 3 3
Belgium 3 3 3 3
Belgium 3 3 3 3
Belgium 3 3 3 3
Belgium 3 3 3 3
Belgium 3 3 3 3
Belgium 3 3 3 3
0
```

```
Case Number 1
Error!: Map contains countries with the same names
Case Number 2
Error!: X(0) is out of allowed bounds
Case Number 3
Error!: Cannot deserialize country from "Belgium 0 3 sd 3"
Case Number 4
France 1325
Portugal 416
Spain 382
Case Number 5
Luxembourg 0
Case Number 6
Belgium 2
Netherlands 2
Case Number 7
Belgium 1
Netherlands 1
Case Number 8
Error!: Two countries are intercepts Netherlands and Belgium at 1-1
Case Number 9
Error!: Top left Y(5) should be lower than bottom right Y(2)
Case Number 10
Error!: Netherlands has no neighbors on the map
Traceback (most recent call last):
  File "/Users/danylokazymyrov/Developer/5-1-tpps-euro-coins/main.py", line 28, in <module>
    main()
  File "/Users/danylokazymyrov/Developer/5-1-tpps-euro-coins/main.py", line 9, in main
    for data in read_test_cases(CASES_FILENAME):
  File "/Users/danylokazymyrov/Developer/5-1-tpps-euro-coins/euroCoins/fs.py", line 19, in read_test_cases
    raise ValueError(
ValueError: Too many countries (21) in case

```
